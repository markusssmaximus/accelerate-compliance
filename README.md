# accelerate-compliance

## Getting started

This tool is designed to allow you to apply a compliance framework to an entire group of projects. Currently, it applies the framework to all sub-groups as well, which can be disabled by changing the include_subgroups argument to false.

All you need to do is:

1. Import python-gitlab by running `pip3 install python-gitlab`
1. Generate your own Personal Access Token with api access for querying projects in a group and assign it to the `YOUR_PAT` global variable.
1. Generate an auth token from a group application with api access and assign it to the `YOUR_AUTH_TOKEN` global variable.
   1. [Generate group application.](https://docs.gitlab.com/ee/integration/oauth_provider.html#group-owned-applications)
   1. [Generate auth token with this application.](https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-flow) 
1. Select your group and assign it to the the `YOUR_GROUP_ID` variable.
1. Select your compliance framework ID and fill in the `YOUR_FRAMEWORK_ID` variable.

Then, run `python3 apply-framework.py`

Piece of :cake:


## TODO:
- [x] @sam Upload recording of how to use the tool
   - [Video Tutorial](https://drive.google.com/file/d/12aAlq7wegBXei-DNQhcSiIt325Yl_DS_/view?usp=sharing)
- [ ] Pass in group ID and framework ID in command line instead of assigning the variable to reduce user error


## Notes:
To obtain your compliance framework ID, navigate to your group's Settings -> Compliance Frameworks and then roll your cursor over the label with the name displayed on it. The ID is the number that appears in the URL on the bottom left of your screen that contains your namespace/compliance_frameworks.
